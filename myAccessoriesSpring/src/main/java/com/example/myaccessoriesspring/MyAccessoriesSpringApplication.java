package com.example.myaccessoriesspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyAccessoriesSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyAccessoriesSpringApplication.class, args);
    }

}
