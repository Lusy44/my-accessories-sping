package com.example.myaccessoriesspring.repository;

import com.example.myaccessoriesspring.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer> {

}
